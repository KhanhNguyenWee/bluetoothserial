package wee.digitall.bluetoothserial.bluetooth

import android.annotation.SuppressLint
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.os.Handler
import android.os.Message
import android.util.Log
import android.view.View
import android.widget.AdapterView.OnItemClickListener
import android.widget.ArrayAdapter
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_server.*
import wee.digitall.bluetoothserial.R
import java.io.ByteArrayOutputStream


/**
 * This ACTIVITY controls Bluetooth to communicate with other devices.
 */
class BluetoothServerActivity : AppCompatActivity() {
    companion object {
        val TAG = "BluetoothServerActivity"
    }

    /**
     * String buffer for outgoing messages
     */
    private var mOutStringBuffer: StringBuffer? = null

    /**
     * Name of the connected device
     */
    private var mConnectedDeviceName: String? = null

    /**
     * Local Bluetooth adapter
     */
    private var mBluetoothAdapter: BluetoothAdapter? = null

    /**
     * Member object for the bluetooth services
     */
    private var mBtService: BluetoothService? = null

    /**
     * Newly discovered devices
     */
    private var mNewDevicesArrayAdapter: ArrayAdapter<String>? = null

    /**
     * The Handler that gets information back from the BluetoothChatService
     */
    @SuppressLint("HandlerLeak")
    private val mHandler: Handler = object : Handler() {
        override fun handleMessage(msg: Message) {
            when (msg.what) {
                Constants.MESSAGE_STATE_CHANGE -> when (msg.arg1) {
                    BluetoothService.STATE_CONNECTED -> {
                        setStatus(getString(R.string.title_connected_to, mConnectedDeviceName))

                    }
                    BluetoothService.STATE_CONNECTING -> setStatus(R.string.title_connecting)
                    BluetoothService.STATE_LISTEN, BluetoothService.STATE_NONE -> setStatus(
                        R.string.title_not_connected
                    )
                }
                Constants.MESSAGE_WRITE -> {
                    val writeBuf = msg.obj as ByteArray
                    Log.d(TAG, "Send - ${writeBuf.size}")

                }
                Constants.MESSAGE_READ -> {
                    val readBuf = msg.obj as ByteArray
                    Log.d(TAG, "Receive - ${readBuf.size}")

                    // construct a string from the valid bytes in the buffer
                    //val receivedBase64String = String(readBuf, 0, msg.arg1)
                    //val imageBytes = Base64.decode(receivedBase64String, Base64.DEFAULT)

                    //val decodedImage = BitmapFactory.decodeByteArray(readBuf, 0, msg.arg1)
                    val bmp = BitmapFactory.decodeByteArray(readBuf, 0, msg.arg1)
                    receivedData.setImageBitmap(bmp)

                }
                Constants.MESSAGE_DEVICE_NAME -> {
                    // save the connected device's name
                    mConnectedDeviceName = msg.data.getString(Constants.DEVICE_NAME)
                    Toast.makeText(
                        applicationContext,
                        "Connected to $mConnectedDeviceName",
                        Toast.LENGTH_SHORT
                    ).show()
                }
                Constants.MESSAGE_TOAST -> Toast.makeText(
                    applicationContext, msg.data.getString(Constants.TOAST),
                    Toast.LENGTH_SHORT
                ).show()
            }
        }
    }

    /**
     * The BroadcastReceiver that listens for discovered devices and changes the title when
     * discovery is finished
     */
    private val mReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            val action = intent.action

            // When discovery finds a device
            if (BluetoothDevice.ACTION_FOUND == action) {
                // Get the BluetoothDevice object from the Intent
                val device =
                    intent.getParcelableExtra<BluetoothDevice>(BluetoothDevice.EXTRA_DEVICE)
                // If it's already paired, skip it, because it's been listed already
                if (device != null && device.bondState != BluetoothDevice.BOND_BONDED) {
                    mNewDevicesArrayAdapter!!.add(device.name + "\n" + device.address)
                }
                // When discovery is finished, change the Activity title
            } else if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED == action) {

                if (mNewDevicesArrayAdapter!!.count == 0) {
                    Toast.makeText(applicationContext, "No paired devices", Toast.LENGTH_SHORT)
                        .show()
                    val noDevices = resources.getText(R.string.none_found).toString()
                    mNewDevicesArrayAdapter!!.add(noDevices)
                }
            }
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_server)

        // Get local Bluetooth adapter
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter()

        // If the adapter is null, then Bluetooth is not supported
        if (mBluetoothAdapter == null) {
            Toast.makeText(this, "Bluetooth is not available", Toast.LENGTH_LONG).show()
        }

        // Initialize array adapters. One for already paired devices and
        // one for newly discovered devices
        val pairedDevicesArrayAdapter = ArrayAdapter<String>(this, R.layout.device_name)
        mNewDevicesArrayAdapter = ArrayAdapter(this, R.layout.device_name)

        // Find and set up the ListView for paired devices
        paired_devices.adapter = pairedDevicesArrayAdapter
        paired_devices.onItemClickListener = mDeviceClickListener

        // Register for broadcasts when a device is discovered
        var filter = IntentFilter(BluetoothDevice.ACTION_FOUND)
        this.registerReceiver(mReceiver, filter)

        // Register for broadcasts when discovery has finished
        filter = IntentFilter(BluetoothAdapter.ACTION_DISCOVERY_FINISHED)
        this.registerReceiver(mReceiver, filter)

        // Get a set of currently paired devices
        val pairedDevices: Set<BluetoothDevice> = mBluetoothAdapter!!.bondedDevices

        // If there are paired devices, add each one to the ArrayAdapter
        if (pairedDevices.isNotEmpty()) {
            title_paired_devices.visibility = View.VISIBLE
            for (device in pairedDevices) {
                pairedDevicesArrayAdapter.add(device.name + "\n" + device.address)
            }
        } else {
            val noDevices = resources.getText(R.string.none_paired).toString()
            pairedDevicesArrayAdapter.add(noDevices)
        }
    }

    override fun onStart() {
        super.onStart()
        if (mBluetoothAdapter == null) {
            return
        }

        // If BT is not on, request that it be enabled.
        // setupUI() will then be called during onActivityResult
        if (!mBluetoothAdapter!!.isEnabled) {
            val enableIntent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
            startActivityForResult(
                enableIntent,
                Constants.REQUEST_ENABLE_BT
            )

        } else if (mBtService == null) {
            setupUI()
        }
    }

    private fun setupUI() {
        Log.d(TAG, "Setup UI server")
        // Initialize the BluetoothService to perform bluetooth connections
        mBtService = BluetoothService(this, mHandler)

        // Initialize the buffer for outgoing messages
        mOutStringBuffer = StringBuffer()


        serverMakeDiscover.setOnClickListener { makeDiscoverable() }

        serverScanDevice.setOnClickListener {
            doDiscover()
            serverScanDevice.visibility = View.GONE
        }

        serverSendData.setOnClickListener { sendSample() }
    }

    override fun onDestroy() {
        super.onDestroy()
        mBtService?.stop()
        // Make sure we're not doing discovery anymore
        if (mBluetoothAdapter != null) {
            mBluetoothAdapter?.cancelDiscovery()
        }
        this.unregisterReceiver(mReceiver)
    }

    override fun onResume() {
        super.onResume()
        // Performing this check in onResume() covers the case in which BT was
        // not enabled during onStart(), so we were paused to enable it...
        // onResume() will be called when ACTION_REQUEST_ENABLE activity returns.
        if (mBtService != null) {
            // Only if the state is STATE_NONE, do we know that we haven't started already
            if (mBtService!!.mState == BluetoothService.STATE_NONE) {
                // Start the Bluetooth chat services
                mBtService!!.start()
            }
        }
    }

    /**
     * Updates the status on the action bar.
     *
     * @param subTitle status
     */
    private fun setStatus(subTitle: CharSequence) {
        val activity = this
        val actionBar = activity.actionBar ?: return
        actionBar.subtitle = subTitle
    }

    /**
     * Updates the status on the action bar.
     *
     * @param resId a string resource ID
     */
    private fun setStatus(resId: Int) {
        val activity = this
        val actionBar = activity.actionBar ?: return
        actionBar.setSubtitle(resId)
    }

    /**
     * Makes this device discoverable for 300 seconds (5 minutes).
     */
    private fun makeDiscoverable() {
        if (mBluetoothAdapter!!.scanMode !=
            BluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE
        ) {
            val discoverableIntent = Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE)
            discoverableIntent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 300)
            startActivity(discoverableIntent)
        }
    }

    /**
     * Sends a sample  image.
     */
    private fun sendSample() {
        // Check that we're actually connected before trying anything
        if (mBtService?.mState != BluetoothService.STATE_CONNECTED) {
            Toast.makeText(applicationContext, R.string.not_connected, Toast.LENGTH_SHORT).show()
            return
        }
        val bitmap = BitmapFactory.decodeResource(resources, R.drawable.img)
        val stream = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream)
        val bitmapData = stream.toByteArray()
        // Get the message bytes and tell the BluetoothChatService to write
        mBtService?.write(bitmapData)

        // Reset out string buffer to zero and clear the edit text field
        mOutStringBuffer!!.setLength(0)

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            /* Constants.REQUEST_CONNECT_DEVICE_SECURE ->
                 // When DeviceListActivity returns with a device to connect
                 if (resultCode == RESULT_OK) {
                     connectDevice(data, true)
                 }
             Constants.REQUEST_CONNECT_DEVICE_INSECURE ->
                 // When DeviceListActivity returns with a device to connect
                 if (resultCode == RESULT_OK) {
                     connectDevice(data, false)
                 }*/
            Constants.REQUEST_ENABLE_BT ->
                // When the request to enable Bluetooth returns
                if (resultCode == RESULT_OK) {
                    // Bluetooth is now enabled, so set up UI session
                    setupUI()
                } else {
                    // User did not enable Bluetooth or an error occurred
                    Log.d(
                        TAG,
                        "BT not enabled"
                    )
                    Toast.makeText(
                        applicationContext, R.string.bt_not_enabled_leaving,
                        Toast.LENGTH_SHORT
                    ).show()
                    this.finish()
                }
        }
    }

    /**
     * Start device discover with the BluetoothAdapter
     */
    private fun doDiscover() {
        Log.d(TAG, "doDiscovery()")
        // If we're already discovering, stop it
        if (mBluetoothAdapter?.isDiscovering == true) {
            mBluetoothAdapter?.cancelDiscovery()
        }
        // Request discover from BluetoothAdapter
        mBluetoothAdapter?.startDiscovery()
    }

    /**
     * The on-click listener for all devices in the ListViews
     */
    private val mDeviceClickListener =
        OnItemClickListener { _, v, _, _ ->
            // Cancel discovery because it's costly and we're about to connect
            mBluetoothAdapter?.cancelDiscovery()

            // Get the device MAC address, which is the last 17 chars in the View
            val info = (v as TextView).text.toString()
            val address = info.substring(info.length - 17)

            val device = mBluetoothAdapter!!.getRemoteDevice(address)
            mBtService?.connect(device, true)
        }

}