package wee.digitall.bluetoothserial

interface OnSerialListener {
    fun getData(string: String)
    fun disConnect()
    fun usbConnect()
}