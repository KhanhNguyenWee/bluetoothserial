package wee.digitall.bluetoothserial.serial

import android.app.Activity
import android.util.Log
import wee.digitall.bluetoothserial.OnSerialListener

class SerialConnectController(private val activity: Activity) : OnSerialListener {
    companion object {
        const val TAG = "SerialConnectController"
        var curDeviceName = ""
        var curPassword = ""

    }

    private var usbSerialInited = false

    //-----
    private var bluetoothSerialController: BluetoothSerialController? = null

    //-----
    private var serialListener: OnSerialListener? = null

    fun setListener(listener: OnSerialListener) {
        serialListener = listener
    }


    fun initBluetoothSerial(deviceName: String, password: String) {
        bluetoothSerialController =
            BluetoothSerialController(activity, deviceName, password)
        bluetoothSerialController?.turnOnBluetooth()
        bluetoothSerialController?.onSerialListener = this
        curDeviceName = deviceName
        curPassword = password
    }

    fun sendDataUSB(value: String) {
        Log.d(TAG, "sendDataUSB: $value")
        bluetoothSerialController?.sendData(value)
    }

    fun onDestroy() {
        Log.d(TAG, "onDestroy")
        bluetoothSerialController?.closeConnect()
    }

    override fun usbConnect() {
        Log.d(TAG, "CONNECTED")
        serialListener?.usbConnect()
        usbSerialInited = true
    }

    override fun disConnect() {
        Log.d(TAG, "DISCONNECTED")
        serialListener?.disConnect()
    }

    override fun getData(string: String) {
        Log.d(TAG, "getData $string")
        serialListener?.getData(string)
    }
}