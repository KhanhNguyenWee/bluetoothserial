package wee.digitall.bluetoothserial.serial

import android.app.Activity
import android.bluetooth.BluetoothDevice
import android.os.Handler
import android.os.HandlerThread
import android.util.Log
import me.aflak.bluetooth.Bluetooth
import me.aflak.bluetooth.interfaces.BluetoothCallback
import me.aflak.bluetooth.interfaces.DeviceCallback
import me.aflak.bluetooth.interfaces.DiscoveryCallback
import me.aflak.bluetooth.reader.LineReader
import wee.digitall.bluetoothserial.OnSerialListener

class BluetoothSerialController(
    activity: Activity,
    private val deviceName: String,
    private val password: String
) : BluetoothCallback {
    companion object {
        const val TAG = "BluetoothSerialControler"
        const val DEVICE_NAME = "HC-06"
        const val DEVICE_PASSWORD = "1234"
        const val TIME_RECONNECT = 5000L
    }

    private var bluetooth = Bluetooth(activity)
    private var handlerThread: HandlerThread? = null
    private var mHandler: Handler? = null
    private var bluetoothDevice: BluetoothDevice? = null
    var onSerialListener: OnSerialListener? = null
    private var isUserDisconnect = false

    init  {
        startBackgroundThread()
        bluetooth.onStart()
        bluetooth.enable()
    }

    fun turnOnBluetooth() {
        mHandler?.post {
            if (bluetooth.isEnabled) {
                val listDevices = bluetooth.pairedDevices
                Log.d(TAG, "ListDevicePaired: $listDevices")
                bluetooth.setDeviceCallback(object : DeviceCallback {
                    override fun onDeviceDisconnected(
                        device: BluetoothDevice?,
                        message: String?
                    ) {
                        Log.d(TAG, "onDeviceDisconnected: ${device?.address} - $message")
                        if (bluetoothDevice != null && device != null) {
                            if (bluetoothDevice!!.address == device.address) {
                                onSerialListener?.disConnect()
                                if (!isUserDisconnect) {
                                    connectDevice(bluetoothDevice!!)
                                }
                            }
                        }
                    }
                    override fun onDeviceConnected(device: BluetoothDevice?) {
                        Log.d(TAG, "onDeviceConnected")
                        onSerialListener?.usbConnect()
                    }
                    override fun onConnectError(
                        device: BluetoothDevice?,
                        message: String?
                    ) {
                        Log.d(TAG, "onConnectError: $message")
                        scanAndConnect()
                    }

                    override fun onMessage(message: ByteArray?) {
                        Log.d(TAG, "onMessage: ${if (message != null) String(message) else ""}")
                        if (message != null) onSerialListener?.getData(String(message))
                    }

                    override fun onError(errorCode: Int) {
                        Log.d(TAG, "onError: Code: $errorCode")
                    }

                })
                for (device in listDevices) {
                    if (device.name == deviceName) {
                        bluetoothDevice = device
                        connectDevice(device)
                        break
                    }
                }
                scanAndConnect()
            }
        }

    }

    private fun scanAndConnect() {
        if (bluetoothDevice == null) {
            bluetooth.startScanning()
            bluetooth.setDiscoveryCallback(object : DiscoveryCallback {
                override fun onDevicePaired(device: BluetoothDevice?) {
                    Log.d(TAG, "onDevicePaired")
                    connectDevice(device)
                }

                override fun onDiscoveryStarted() {
                    Log.d(TAG, "onDiscoveryStarted")
                }

                override fun onDeviceUnpaired(device: BluetoothDevice?) {
                    Log.d(TAG, "onDeviceUnpaired $device")
                }

                override fun onError(errorCode: Int) {
                    Log.e(TAG, "onError: Code $errorCode")
                    bluetoothDevice = null
                    scanAndConnect()
                }

                override fun onDiscoveryFinished() {
                    Log.d(TAG, "onDiscoveryFinished")
                    if (bluetoothDevice == null) bluetooth.startScanning()
                }

                override fun onDeviceFound(device: BluetoothDevice?) {
                    Log.d(TAG, "onDeviceFound ${device?.name ?: device?.address}")
                    if (device != null) {
                        if (device.name == deviceName) {
                            bluetooth.stopScanning()
                            bluetoothDevice = device
                            pairDevice(device)
                        }
                    }
                }
            })
        }
    }

    private fun connectDevice(device: BluetoothDevice?) {
        device ?: return
        mHandler?.post {
            bluetoothDevice = device
            Log.d(TAG, "Connecting Serial Device: $bluetoothDevice")
            bluetooth.setReader(LineReader::class.java)
            bluetooth.connectToAddress(bluetoothDevice!!.address, true, true).apply {
                Log.d(TAG, "Connected Serial Device: $bluetoothDevice")
            }
        }
    }

    private fun pairDevice(device: BluetoothDevice) {
        mHandler?.post {
            bluetoothDevice = device
            Log.d(TAG, "Pairing Device: $bluetoothDevice")
            try {
                bluetooth.pair(bluetoothDevice)
                bluetooth.pair(bluetoothDevice, password)
            } catch (e: Exception) {
                Log.d(TAG, "${e.message}")
            }
        }
    }

    fun sendData(str: String) {
        mHandler?.post {
            if (bluetoothDevice != null) {
                try {
                    bluetooth.send(str)
                    Log.d(TAG, "SendData: $str")
                } catch (e: Exception) {
                    Log.d(TAG, "${e.message}")
                }
            }
        }
    }

    fun disConnectDevice() {
        isUserDisconnect = true
        bluetooth.socket.close()
    }

    fun closeConnect() {
        isUserDisconnect = true
        disConnectDevice()
        bluetooth.disable()
        bluetooth.disconnect()
        bluetooth.onStop()
        stopBackgroundThread()
    }

    override fun onUserDeniedActivation() {
        Log.d(TAG, "onUserDeniedActivation")
    }

    override fun onBluetoothOff() {
        Log.d(TAG, "onBluetoothOff")
    }

    override fun onBluetoothOn() {
        Log.d(TAG, "onBluetoothOn")
    }

    override fun onBluetoothTurningOn() {
        Log.d(TAG, "onBluetoothTurningOn")
    }

    override fun onBluetoothTurningOff() {
        Log.d(TAG, "onBluetoothTurningOff")
    }

    private fun startBackgroundThread() {
        handlerThread = HandlerThread(TAG)
        handlerThread?.start()
        mHandler = Handler(handlerThread!!.looper)
    }

    private fun stopBackgroundThread() {
        try {
            handlerThread?.quitSafely()
            handlerThread?.join()
        } catch (e: Exception) {
            Log.d(TAG, "${e.message}")
        }
    }

}

