package wee.digitall.bluetoothserial

import android.content.Intent
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.os.Bundle
import android.util.Base64
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import wee.digitall.bluetoothserial.bluetooth.BluetoothServerActivity
import wee.digitall.bluetoothserial.serial.SerialConnectController
import java.io.ByteArrayOutputStream

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        btnClient.setOnClickListener {
           Toast.makeText(applicationContext,"Developing",Toast.LENGTH_SHORT).show()
        }

        btnServer.setOnClickListener {
           startActivity(Intent(this,BluetoothServerActivity::class.java))
        }
    }


}